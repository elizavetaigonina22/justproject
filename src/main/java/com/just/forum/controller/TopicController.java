package com.just.forum.controller;

import com.just.forum.data.mappers.UserMapper;
import com.just.forum.db.TopicProvider;
import com.just.forum.domain.entity.PersonEntity;
import com.just.forum.domain.entity.TopicEntity;
import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.service.TopicService;
import com.just.forum.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

//http://localhost:8080/topic

@RestController
@RequestMapping("topic")
public class TopicController {

    private final TopicService topicService;

    private final UserService userService;

    @Autowired
    public TopicController(TopicService topicService, UserService userService) {
        this.topicService = topicService;
        this.userService = userService;
    }

    public static TopicProvider topicProvider = new TopicProvider();

    @GetMapping
    public List<TopicEntity> list() {
        List<TopicEntity> topics = topicService.getAllEntity();
        return topics;
    }

    @GetMapping("get")
    public TopicEntity getOneTopic(@PathParam("id") String id) {
        TopicEntity topic = topicService.findTopicById(id);
        return topic;
    }

    @PutMapping("create")
    public void create(@RequestBody TopicEntity entity) {
        UserEntity creator = getCurrentUser();

        PersonEntity creatorPersonEntity = UserMapper.toPersonEntity(creator);
        entity.setCreator(creatorPersonEntity);

        topicService.createTopic(entity);
    }

    @PutMapping("edit")
    public void edit(@PathParam("id") String id, @RequestBody TopicEntity entity) {
        entity.setId(id);
        topicService.editTopic(entity);
    }

    @DeleteMapping
    public void remove(@PathParam("id") String id) {
        topicService.deleteTopic(id);
    }

    private UserEntity getCurrentUser() {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            return userService.findByUsername(userName);
        } catch (Throwable e) {
            return null;
        }
    }
}