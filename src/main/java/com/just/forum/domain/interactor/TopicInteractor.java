package com.just.forum.domain.interactor;

import com.just.forum.domain.repository.CommentRepo;
import com.just.forum.domain.repository.TopicRepo;
import com.just.forum.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class TopicInteractor {

    private TopicRepo topicRepo;
    private UserRepository userRepository;
    private CommentRepo commentRepo;

    @Autowired
    public TopicInteractor(TopicRepo topicRepo, UserRepository userRepository, CommentRepo commentRepo) {
        this.topicRepo = topicRepo;
        this.userRepository = userRepository;
        this.commentRepo = commentRepo;
    }
}