package com.just.forum.domain.entity;

import java.util.Date;

public class CommentEntity {
    private String id;
    private String comment;
    private Date date;
    private PersonEntity creator;

    public CommentEntity() {}

    public CommentEntity(String id, String comment, Date date, PersonEntity creator) {
        this.id = id;
        this.comment = comment;
        this.date = date;
        this.creator = creator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public PersonEntity getCreator() {
        return creator;
    }

    public void setCreator(PersonEntity creator) {
        this.creator = creator;
    }
}
