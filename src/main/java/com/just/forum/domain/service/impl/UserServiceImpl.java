package com.just.forum.domain.service.impl;

import com.just.forum.data.mappers.UserMapper;
import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.repository.UserRepository;
import com.just.forum.domain.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserEntity register(UserEntity user) {
        try {
            findByUsername(user.getUserName());
            throw new RuntimeException("User already created");
        } catch (UsernameNotFoundException ex) {
            user.setId(UUID.randomUUID().toString());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            UserDbEntity userDbEntity = UserMapper.toDbEntity(user);
            userRepository.save(userDbEntity);
            log.info("IN register - user: {} successfully registered", userDbEntity);
            return user;
        }
    }

    @Override
    public List<UserEntity> getAll() {
        List<UserEntity> result = new ArrayList<>();
        List<UserDbEntity> dbEntities = userRepository.findAll();
        for (int i = 0; i < dbEntities.size(); i++) {
            UserEntity currentUserEntity = UserMapper.toEntity(dbEntities.get(i));
            result.add(currentUserEntity);
        }
        log.info("IN getAll - {} users found", result.size());
        return result;
    }

    @Override
    public UserEntity findByUsername(String username) throws UsernameNotFoundException {

        UserDbEntity loadedUserDbEntity = userRepository.findAll().stream()
                .filter(userDbEntity -> userDbEntity.getUserName().equals(username))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("User with name " + username + " not found"));
        return UserMapper.toEntity(loadedUserDbEntity);
    }

    @Override
    public UserEntity findById(String id) {
        UserDbEntity userDbEntity = userRepository.findById(id).orElse(null);
        if (userDbEntity == null) {
            log.warn("IN findById - no user found by id: {}", id);
            return null;
        }

        UserEntity result = UserMapper.toEntity(userDbEntity);

        log.info("IN findById - user: {} found by id: {}", result);
        return result;
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted");
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = findByUsername(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new User(userEntity.getUserName(), userEntity.getPassword(), new ArrayList<>());
    }
}