package com.just.forum.db;

import com.just.forum.domain.entity.TopicEntity;
import com.just.forum.exceptions.NotFoundException;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TopicProvider {

    private List<TopicEntity> topicList = new ArrayList<>();

    public List<TopicEntity> getAll() {
        return topicList;
    }

    public TopicEntity getOne(String id) {
        for (int i = 0; i < topicList.size(); i++) {
            TopicEntity current = topicList.get(i);
            if (current.getId().equals(id)) {
                return current;
            }
        }
        throw new NotFoundException();
    }

    public void create(TopicEntity newTopic) {
        if (ObjectUtils.isEmpty(newTopic.getId())) {
            newTopic.setId(UUID.randomUUID().toString());
        }
        newTopic.setComments(new ArrayList<>());
        topicList.add(newTopic);
    }

    public void edit(TopicEntity updatedTopic) {
        TopicEntity oldTopic = getOne(updatedTopic.getId());
        oldTopic.setTitle(updatedTopic.getTitle());
        oldTopic.setDescription(updatedTopic.getDescription());
        delete(oldTopic.getId());
        create(oldTopic);
    }

    public void delete(String id) {
        int indexCurrentTopic = -1;
        for (int i = 0; i < topicList.size(); i++) {
            if (topicList.get(i).getId().equals(id)) {
                indexCurrentTopic = i;
                break;
            }
        }
        if (indexCurrentTopic >= 0) {
            topicList.remove(indexCurrentTopic);
            return;
        }
        throw new NotFoundException();
    }

}
