package com.just.forum.domain.repository;

import com.just.forum.db.entity.TopicDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;

//db username postgres
//db password 123
//db port 5432
public interface TopicRepo extends JpaRepository<TopicDbEntity, String> {
}
