package com.just.forum.domain.repository;

import com.just.forum.db.entity.UserDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDbEntity, String> {
}