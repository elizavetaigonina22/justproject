package com.just.forum.controller;

import com.just.forum.domain.entity.user.UserEntity;
import com.just.forum.domain.service.UserService;
import com.just.forum.security.config.JwtTokenUtil;
import com.just.forum.security.model.JwtRequest;
import com.just.forum.security.model.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService jwtInMemoryUserDetailsService;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController() {

    }

    @GetMapping("home")
    public String home() {
        return "Hello to home page";
    }

    @PutMapping("register")
    public void register(@RequestBody UserEntity userEntity) {
        jwtInMemoryUserDetailsService.register(userEntity);
    }

    @PostMapping("login")
    public ResponseEntity<?> auth(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = jwtInMemoryUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }

    @GetMapping("users")
    public List<UserEntity> getAllUsers() {
        return jwtInMemoryUserDetailsService.getAll();
    }

    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            UserEntity currentUser = jwtInMemoryUserDetailsService.findByUsername(username);
            Boolean passwordIsTrue = passwordEncoder.matches(password, currentUser.getPassword());
            if (!passwordIsTrue) {
                throw new RuntimeException("Неверный пароль");
            }
        } catch (UsernameNotFoundException e) {
            throw new RuntimeException("Пользователь с таким логином еще не создан");
        } catch (Throwable e) {
            throw new RuntimeException("Ошибка авторизации");
        }
    }
}
