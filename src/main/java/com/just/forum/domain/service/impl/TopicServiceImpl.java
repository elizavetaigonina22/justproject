package com.just.forum.domain.service.impl;

import com.just.forum.data.mappers.TopicMapper;
import com.just.forum.db.entity.CommentDbEntity;
import com.just.forum.db.entity.TopicDbEntity;
import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.TopicEntity;
import com.just.forum.domain.repository.CommentRepo;
import com.just.forum.domain.repository.TopicRepo;
import com.just.forum.domain.repository.UserRepository;
import com.just.forum.domain.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TopicServiceImpl implements TopicService {

    private TopicRepo topicRepo;
    private UserRepository userRepository;
    private CommentRepo commentRepo;


    @Autowired
    public TopicServiceImpl(TopicRepo topicRepo, UserRepository userRepository, CommentRepo commentRepo) {
        this.topicRepo = topicRepo;
        this.userRepository = userRepository;
        this.commentRepo = commentRepo;
    }

    @Override
    public List<TopicEntity> getAllEntity() {
        List<TopicDbEntity> dbEntityList = topicRepo.findAll();
        List<TopicEntity> result = new ArrayList<>();
        for (int i = 0; i < dbEntityList.size(); i++) {
            TopicDbEntity currentDbTopic = dbEntityList.get(i);
            UserDbEntity personDbEntity = userRepository.findById(currentDbTopic.getCreatorPersonId()).orElse(null);
            TopicEntity currentTopic = TopicMapper.dbToEntityTopic(currentDbTopic, personDbEntity);
            List<CommentEntity> comments = getAllComments(currentTopic.getId());
            currentTopic.setComments(comments);
            result.add(currentTopic);
        }
        return result;
    }

    @Override
    public TopicEntity findTopicById(String topicId) {
        TopicDbEntity dbTopic = topicRepo.findById(topicId).orElseThrow();
        UserDbEntity creator = userRepository.findById(dbTopic.getCreatorPersonId()).orElse(null);
        TopicEntity topic = TopicMapper.dbToEntityTopic(dbTopic, creator);
        List<CommentEntity> comments = getAllComments(topicId);
        topic.setComments(comments);
        return topic;
    }

    @Override
    public void createTopic(TopicEntity topic) {
        String newId = UUID.randomUUID().toString();
        topic.setId(newId);
        TopicDbEntity dbTopic = TopicMapper.toDbTopic(topic);
        topicRepo.save(dbTopic);
    }

    @Override
    public void editTopic(TopicEntity topic) {
        TopicDbEntity dbEntity = topicRepo.findById(topic.getId()).orElseThrow();
        dbEntity.setTitle(topic.getTitle());
        dbEntity.setDescription(topic.getDescription());
        topicRepo.save(dbEntity);
    }

    @Override
    public void deleteTopic(String topicId) {
        TopicDbEntity dbEntity = topicRepo.findById(topicId).orElseThrow();
        List<String> dbEntityCommentIds = dbEntity.getCommentIds();
        for (int i = 0; i < dbEntityCommentIds.size(); i++) {
            commentRepo.deleteById(dbEntityCommentIds.get(i));
        }
        topicRepo.deleteById(topicId);
    }

    private List<CommentEntity> getAllComments(String topicId) {
        List<CommentEntity> commentsResult = new ArrayList<>();
        List<CommentDbEntity> allDBComments = commentRepo.findAll();
        for (int i = 0; i < allDBComments.size(); i++) {
            CommentDbEntity currentDBComment = allDBComments.get(i);
            if (currentDBComment.getTopicId().equals(topicId)) {
                UserDbEntity commentCreator = userRepository.findById(currentDBComment.getCreatorId()).orElse(null);
                CommentEntity currentComment = TopicMapper.dbToEntityComment(currentDBComment, commentCreator);
                commentsResult.add(currentComment);
            }
        }
        return commentsResult;
    }
}
