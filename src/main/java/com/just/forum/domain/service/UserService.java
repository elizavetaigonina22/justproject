package com.just.forum.domain.service;

import com.just.forum.domain.entity.user.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    UserEntity register(UserEntity user);

    List<UserEntity> getAll();

    UserEntity findByUsername(String username) throws Throwable;

    UserEntity findById(String id);

    void delete(String id);
}
