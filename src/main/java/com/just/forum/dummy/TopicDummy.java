package com.just.forum.dummy;

import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.PersonEntity;
import com.just.forum.domain.entity.TopicEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TopicDummy {

    public List<TopicEntity> topics;

    public TopicDummy() {
        initTopics();
    }

    public List<TopicEntity> getAllTopics() {
        return topics;
    }

    private void initTopics() {
        topics = new ArrayList<TopicEntity>();

//        Topic 1

        TopicEntity topic1 = new TopicEntity();
        topic1.setId("TOPIC_ID_1");
        topic1.setTitle("Обсуждение 1");
        topic1.setDescription(LoremIpsum.LOREM_LONG);

        PersonEntity personOne = new PersonEntity();
        personOne.setId("PERSON_ID_1");
        personOne.setName("Ivan");
        personOne.setSurname("Ivanov");
        topic1.setCreator(personOne);

        List<CommentEntity> comments = new ArrayList<CommentEntity>();
        CommentEntity commentOneOne = new CommentEntity("COMMENT_ID_1_1", LoremIpsum.LOREM_LONG, new Date(), new PersonEntity("PERSON_ID_2", "Vladimir", "Vladimirov"));
        CommentEntity commentOneTwo = new CommentEntity("COMMENT_ID_1_2", LoremIpsum.LOREM_LONG, new Date(), new PersonEntity("PERSON_ID_3", "Pasha", "Pashov"));

        comments.add(commentOneOne);
        comments.add(commentOneTwo);

        topic1.setComments(comments);

        topics.add(topic1);
//        Topic 2

        TopicEntity topic2 = new TopicEntity();
        topic2.setId("TOPIC_ID_2");
        topic2.setTitle("Обсуждение 2");
        topic2.setDescription(LoremIpsum.LOREM_LONG);

        PersonEntity personTwo = new PersonEntity();
        personOne.setId("PERSON_ID_2");
        personOne.setName("Misha");
        personOne.setSurname("Mishov");
        topic2.setCreator(personTwo);

        List<CommentEntity> commentsTwo = new ArrayList<CommentEntity>();
        CommentEntity commentTwoOne = new CommentEntity("COMMENT_ID_1_1", LoremIpsum.LOREM_LONG, new Date(), new PersonEntity("PERSON_ID_2", "Vladimir", "Vladimirov"));
        CommentEntity commentTwoTwo = new CommentEntity("COMMENT_ID_1_2", LoremIpsum.LOREM_LONG, new Date(), new PersonEntity("PERSON_ID_3", "Pasha", "Pashov"));

        commentsTwo.add(commentTwoOne);
        commentsTwo.add(commentTwoTwo);

        topic2.setComments(commentsTwo);

        topics.add(topic2);
    }
}
