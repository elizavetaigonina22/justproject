package com.just.forum.data.mappers;

import com.just.forum.db.entity.CommentDbEntity;
import com.just.forum.db.entity.TopicDbEntity;
import com.just.forum.db.entity.UserDbEntity;
import com.just.forum.domain.entity.CommentEntity;
import com.just.forum.domain.entity.PersonEntity;
import com.just.forum.domain.entity.TopicEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TopicMapper {

    public static TopicDbEntity toDbTopic(TopicEntity topic) {
        String id = topic.getId();
        String title = topic.getTitle();
        String description = topic.getDescription();
        List<CommentDbEntity> comments = new ArrayList<>();
        topic.getComments().forEach(comment -> comments.add(toDbComment(comment, id)));

        TopicDbEntity result = new TopicDbEntity();
        result.setId(id);
        result.setTitle(title);
        result.setDescription(description);
        result.setCreatorPersonId(topic.getCreator().getId());
        return result;
    }

    public static CommentDbEntity toDbComment(CommentEntity comment, String topicId) {
        String id = comment.getId();
        String commentText = comment.getComment();
        Date date = comment.getDate();
        CommentDbEntity result = new CommentDbEntity();
        result.setId(id);
        result.setTopicId(topicId);
        result.setComment(commentText);
        result.setDate(date);
        result.setCreatorId(comment.getCreator().getId());
        return result;
    }


    public static TopicEntity dbToEntityTopic(TopicDbEntity topic, UserDbEntity creatorDb) {
        String id = topic.getId();
        String title = topic.getTitle();
        String description = topic.getDescription();
        PersonEntity creator = dbToEntityPerson(creatorDb);
//        List<CommentEntity> comments = new ArrayList<>();
//        topic.getComments().forEach(comment -> comments.add(dbToEntityComment(comment)));

        TopicEntity result = new TopicEntity();
        result.setId(id);
        result.setTitle(title);
        result.setDescription(description);
        result.setCreator(creator);
//        result.setComments(comments);
        return result;
    }

    public static CommentEntity dbToEntityComment(CommentDbEntity comment, UserDbEntity creatorDb) {
        String id = comment.getId();
        String commentText = comment.getComment();
        Date date = comment.getDate();
        PersonEntity creator = dbToEntityPerson(creatorDb);
        CommentEntity result = new CommentEntity();
        result.setId(id);
        result.setComment(commentText);
        result.setDate(date);
        result.setCreator(creator);
        return result;
    }

    public static PersonEntity dbToEntityPerson(UserDbEntity person) {
        if (person == null) {
            return null;
        }
        String id = person.getId();
        String name = person.getFirstName();
        String surname = person.getLastName();
        PersonEntity result = new PersonEntity();
        result.setId(id);
        result.setName(name);
        result.setSurname(surname);
        return result;
    }
}
